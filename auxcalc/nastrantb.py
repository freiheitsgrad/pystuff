from math import sqrt, pi


def SurfContactParams (group, nodename, gapdist, meshsize):
    '''
    Calculates and returns for Inventor Nastran
    1) 'Max. Activtion Distance': Spaltabstand, sowie Meshgröße aus Meshliste des Sekundärobjekts!
    2) 'Penetration Surface Offset'
    https://www.autodesk.com/support/technical/article/caas/sfdcarticles\
    /sfdcarticles/Understanding-maximum-activation-distance-and-contact-type-in-a-Simulation.html'''
    
    fname = 'SCP'
    
    K = 1.15 # Adjustment Factor - See link:"..between 1.1 and 1.2"
    pensurfoffset = gapdist
    maxactdist = sqrt (gapdist**2 + meshsize**2) * K
        
    ResultsOut(fname, group, nodename, pensurfoffset, maxactdist)
    
    return ()

def ResultsOut (func_name, group, nodename, param1, param2):
    
    if func_name == 'SCP':
        
        resstring1 = 'D.fläch. vers.'
        resstring2 = 'Max Akt.abs.'
    
    elif func_name == 'RP':
               
        resstring1 = 'A'
        resstring2 = 'B'
        
    #print ()
    #print ('\'{0}\''.format(nodename))
    print ('{0},\t{1}'.format(group, nodename))
    #print ('Spaltabstand: {0}, Meshgröße \'Sekundäres Objekt (!)\': {1}'.format(gapdist, meshsize))
    print ('\t{0}:\t{1:1.1f}mm,\t{2}:\t{3:1.1f}mm'.format(resstring1, param1, resstring2, param2))
    print ('')
        
    

def d_spannung (gewinde):
    '''Spannungsdurchmesser, (mm)
       gewinde[0]= d2: Flankendurchmesser [mm]/ pitch diameter
       gewinde[1]= d3: Kerndurchmesser [mm]/ core diameter 
    '''

    d_S = (gewinde[0]+gewinde[1]) / 2
    return d_S

def A_spannung(gewinde):
    '''Spannungsquerschnitt, (mm^2)'''
    
    S = pi/4  * (d_spannung(gewinde))**2 #[mm]
    return S

def I_polar (gewinde):
    '''polares Flächenträgheitsmoment (mm^4)'''
    
    I_p =pi/32 * d_spannung(gewinde)**4
    return I_p

def RodParams (nodename, gew_bez, gewinde):
    '''
    Spannungsdurchmesser, (mm)
    gew_bez als String
    gewinde als liste mit
    gewinde[0]= d2: Flankendurchmesser [mm]/ pitch diameter
    gewinde[1]= d3: Kerndurchmesser [mm]/ core diameter
      
    '''
    
    fname = 'RP'
    
    #A_erg = A_spannung (gewinde)
    #I_erg = I_polar (gewinde)
    
    print ()
    print ('\'{0}\' ({1}\)'.format(nodename, gew_bez))
    print ()
    print ('d2 (Flankendurchmesser): {0}, d3 (Kerndurchmesser): {1}'.format(gewinde[0], gewinde[1]))
    print ('Spannungsquerschnitt für Gewinde {0}= {1:1.3f} mm^2'.format(gewinde, A_spannung(gewinde)))
    print ('Pol. Flächenträgheitsmom. für Gewinde {0}= {1:1.3f} mm^4'.format(gewinde, I_polar(gewinde)))
    print ('--')
