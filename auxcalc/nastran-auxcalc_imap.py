# -*- coding: utf-8 -*-
"""
Created on Mon Sep 16 16:50:32 2024

@author: supex164
"""
from nastrantb import *

# RodParams (nodename, gew_bez, gewinde):
RodParams('Verbinder_mot-a', 'Tr 60 x 9', [55.5, 50.])


# SurfContactParams (nodename, gapdist, meshsize)
# gapdist: Spaltabstand im Modell, da die Nodes des
# _secondary_ Objects zum _primary_ 'gezogen' wern müssen
# meshsize: Meshgröße des _secondary_ objects taken from _mesh table_!

# Laufkatze
SurfContactParams('laufkatze', '[M] laufkatze-a/ b_rippen-platte', 5, 2.2)
SurfContactParams('laufkatze', '[M] laufkatze-a/ b_klotz-trager', 1, 4)
SurfContactParams('laufkatze', '[M] laufkatze-a/ b_stuetzrollen', 5, 5.3)
SurfContactParams('laufkatze', '[M] laufkatze-li/ re_rippen-klotz_oben/ unten', 0, 2.25)
SurfContactParams('laufkatze', '[M] laufkatze_li/ re_rippe-platte_oben/ unten 1/2', 5, 3.0)

# Schubgabel
SurfContactParams('schubgabel', '[M] schubgabel_mittelsteg-rippen-a_mitte', 0, 3.5)
SurfContactParams('schubgabel', '[M] schubgabel_seite-a/ b-stegblech-oben/ unten', 0, 3.0)
SurfContactParams('schubgabel', '[M] schubgabel_traeger-a/ b_mittelsteg-oben/ unten', 1, 39.2)
SurfContactParams('schubgabel', '[M] schubgabel_traeger-a/ b_mittelsteg-mitte', 0, 39.2)
SurfContactParams('schubgabel', 'schubgabel_schubplatte-rippen_oben/unten', 10, 3.0)
SurfContactParams('schubgabel', '[M] schubgabel_schubplatte_rippen-traeger_oben/ unten', 0, 3.0)
SurfContactParams('schubgabel', '[M] schubgabel_traegerplatte-a/ b-traeger', 10, 62.4)
SurfContactParams('schubgabel', '[M] schubgabel_traegerplatte-a/ b-schubtraeger', 10, 45.4)
SurfContactParams('schubgabel', '[M] schubtraeger_rippen-mitte', 0, 10)
SurfContactParams('schubgabel', '[M] schubtraeger_rippen-oben/ unten', 0, 10)
