# -*- coding: utf-8 -*-
"""
Created on Thu Nov  7 09:36:54 2019

@author: seimetz
"""

### Sources ###

#Source (1): Radiation View Factors/ Isidoro Martinez
#http://webserver.dmt.upm.es/%7Eisidoro/tc3/Radiation%20View%20factors.pdf

#Source (2): TAK2000, Manual Release 5.0 15 JAN 2011


from math import *

#########################
### Conductive Conductors
#########################

def G_P (nnumber, tco1, mdistG1, A1, tco2, mdistG2, A2):
    # Parallel Conductor
    
    #G = tco * A / L
    G1 = tco1 * A1 / mdistG1
    G2 = tco2 * A2 / mdistG2
    
    GP = G1 + G2
    print('Parallel conductance for conductor #{0}: {1:5.1f} [W/K]'.format(nnumber, GP))
    return ('Done.')

def G_S (nnumber, tco1, mdistG1, A1, tco2, mdistG2, A2):
    # Series Conductor
    
    #G = tco * A / L
    G1 = tco1 * A1 / mdistG1
    G2 = tco2 * A2 / mdistG2
    
    GS = 1/ (1/ G1 + 1/ G2)
    print('Series conductance for conductor #{0}: {1:5.1f} [W/K]'.format(nnumber, GS))
    return ('Done.')

########################
### Radiative Conductors
########################
#Source (2): Radiation View Factors/ Isidoro Martinez
#http://webserver.dmt.upm.es/%7Eisidoro/tc3/Radiation%20View%20factors.pdf

def GRad (A, e_eff, F12, sigma):

    G = A * e_eff * F12 * sigma

    return (G)


########################
### View Factors
########################

def FV_erp (W1, W2, H):
    # Even Rectangular Plates, (1) S.23
    # Between parallel equal rectangular plates of size W1*W2 separated at
    # distance H, with x= W1/H and y= W2/H.
    
    # Approach: Radiative coupling between outer instrument surface and MLI

    # Test source example
    #x=1
    #y=1

    x = W1 / H
    y = W2 / H

    x1 = sqrt(1+x**2)
    y1 = sqrt(1+y**2)

    A = 1/(pi*x*y)
    B = log( (x1**2*y1**2)/ ((x1**2)+(y1**2)-1) )
    C = 2*x*(y1*atan(x/y1)-atan(x))
    D = 2*y*(x1*atan(y/x1)-atan(y))

    F12 = A*(B+C+D)
    return (F12)

def FV_rp2urp (H, W, L):
    # Rectangular plate to uneven rectangular plate, (1) S.27
    # Approach: Rad. coupling between outer instr wall and i/f panel.
    # Caution: Underestimates coupling because plane doesn't extend from instr chassis
    # in 'L', see example in source.
    
    # Test source example
    #h=1
    #=1

    w = W/L
    h = H/L
    a = (1+h**2)*(1+w**2)/(1+h**2+w**2)
    b = w**2 * (1 + h**2 + w**2) / ((1 + w**2) * (h**2 + w**2))
    c = h**2 * (1 + h**2 + w**2) / ((1 + h**2) * (h**2 + w**2)) 

    A = 1/(pi*w)
    B = h*atan(1/h)
    C = w*atan(1/w)
    D = sqrt(h**2 + w**2) * atan (1 / sqrt(h**2 + w**2))
    E = 1/4 * log (pow(a*b, w**2) * pow(c, h**2))

    F12 = A*(B+C-D+E)
    return (F12)

def FV_p2ts (beta, H, R):

# Plate to tilted sphere, (1) S.10
# Approach: Rad. coupling from instr wall to space.
# Caution: Underestimates coupling because plane doesn't extend from instr chassis
# in 'L', see example in source.

    # Test source example
    #h=1
    #=1


    return (F12)

def FV_p2d (H, R):

# Patch to a parallel disc, (1) S.22
# Approach: Rad. coupling from instr wall to sun.

    # Test source example
    #h=1
    #=1

    h = H/R

    F12= 1 / (1+h**2)

    return (F12)

def FV_p2sc (alpha):

# Patch to a sperical cap, (1) S.22
# Approach: Rad. coupling from instr. wall to space.

    # Test source example
    #h=1
    #=1

    #h = H/R

    F12= sin(radians(alpha))**2

    return (F12)

