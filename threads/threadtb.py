# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 17:01:11 2020

@author: seimetz
"""

#from math import pi, log, cos, radians, sqrt
import xlrd3 as xlrd

class Thread:
    
    '''Various actions for thread identification.'''
    
    def __init__(self, D_out, pitch):
        
        # Enter Dia from mm in inch
        self.do = D_out
        self.p = pitch
        
        self.t = self.DataImport()
        
        
    def DataImport (self):
        '''Import Autodesk's 'thread.xls which need to recide in the same dir. '''
        
        wb = xlrd.open_workbook('thread.xls')
        
        self.sheet = wb.sheet_by_index(0)
        
        print('File \'thread.xls\' imported.')
        
        
        return ()
    
    def Search (self, deviation):
        
        
        #Deviation
        dev= float(deviation)
        #print(type(dev))
        
        # Set min/ max major diameter columns
        col_max = 6 
        col_min = 7
        # Set designator column
        col_desig = 2
        
        print('{0} with {1} rows.'.format(self.sheet.name, self.sheet.nrows))
        print('Searching for dOut={:6.3f}mm +/- {:3.2f}%\n'.format((self.do), dev))
        
        for n in range(3, self.sheet.nrows):
            #Current max. major diameter
            val_min = self.sheet.cell_value(rowx=n, colx=col_min)
            #Current min. major diameter
            val_max = self.sheet.cell_value(rowx=n, colx=col_max)
            
            
            if (val_min > (self.do/25.4-self.do/25.4*dev) and (val_max < (self.do/25.4+self.do/25.4*dev))):
                
                # Split string of column col_desig at the '-'
                st = self.sheet.cell_value(rowx=n, colx=col_desig).split('-')
                # Split it again at the '' (space). Should return list with ['Revolutions/inch', 'thread type']
                st = st[1].split()
                # Calculate metric pitch.               
                p = 25.4 / float(st[0])
                
                print('Row {}: {}.'.format (n+1, self.sheet.cell_value(rowx=n, colx=col_desig)))
                
                print('\t\td_Out_max={:6.3f}mm, ({:6.4f}\'\')'.format (val_max*25.4, val_max))
                print('\t\td_Out_min={:6.3f}mm, ({:6.4f}\'\')'.format (val_min*25.4, val_min))
                print('\t\tp={:6.3f}mm ({} turns per inch)\n'.format (p, st[0]))    
                print('\t\tRef: d_Out:{:6.3f}, p:{:6.3f}'.format(self.do, self.p))
                print('\t\tDeviation rel. to measured: d_Out_max:{:6.2f}%, d_Out_min:{:6.2f}%, p:{:6.2f}%\n'.\
                      format(val_max*25.4-self.do/(self.do), val_min*25.4-self.do/(self.do), (p-self.p)/self.p)) 
                
            else:
                pass
        
        return ()
    
 