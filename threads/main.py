# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 08:54:06 2020

@author: seimetz

Precautions:
-Utilizes Autodesk's 'thread.xls'. Means - you need that in the same directory.
-Relies on 'xlrd3' to read the xls. May 'pip install xlrd3' works on your setup.

"""

import threadtb as ttb

#Enter measured outer bolt diameter, thread pitch in mm
outer_diam, pitch = 2.7, 0.635
t=ttb.Thread(outer_diam, pitch)

#Invoke with deviation of entered values, e.g. 0.1: ')
deviation= 0.05
t.Search(deviation)