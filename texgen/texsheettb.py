# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 11:05:56 2021

@author: seimetz
"""
#https://openpyxl.readthedocs.io/en/stable/index.html
from openpyxl import load_workbook

#endings = ['jpg', 'png']

def GenIncludegraphics(current_line, endings):
    """
    Takes list of strings and iterates until element of 'ending' matches list
    entry. Takes that entry and generates tex-string returning it as list.
    """
    pic = False
    out=''
    i=0
    
    # Iterate over every cell in given line.
    for cell in current_line:
        # Iterate over all defined picture endings.
        for i in range(0, len(endings)):
            # Set flag if entry matches.
            if endings[i] in cell:
                pic = True
                print("pic flag")
                # Save the matching entry.
                pic_type = endings[i]
        
        if pic == True:
            
            print("pic case")
            pic_file = cell.split(pic_type, 1)[0] + pic_type
            
            #print('Gefunden {}, {}'.format(.end[n], snip[i]))
            tex_code = '\\\includegraphics[width=5cm]{media/' + pic_file + '}' 
            out = out + ' ' + tex_code
            pic = False
        
        else:
            print("else case")
            out = out + ' ' + cell
    out = out.replace("\n", "")
    return out

def Xlsx2Tex(file, endings):
    """ Import data from Windows-CSV and transform it into copy/pasteable
        Latex longtable format. Enter filename below. Use <save as> (CSB UTF8) Option
        in Excel to generate the file."""
    
    i, n = 0, 0
    line=[]
    newtablerow, fcell = '', ''
    
    pic_column = 3
    last_column = 5
    
    # Opening results-file in 'write' deletes (!) all content.
    # Just being picky.
    with open('out.tex', 'w'): pass
    # Opening results-file for appending content.
    with open('out.tex', 'a') as output_file:

        #file ='mrpa-assy_1111-em_log.xlsx'
        
        wb = load_workbook(file)
        ws = wb['Step-by-step procedure']
        #dim = ws.dimensions
        
        for row in ws.iter_rows(min_row=1, values_only=True):
                   
            # Change tuple into list.
            # 'openpyxl' returns immutable tuple from excel. Empty cells in
            # Excel are returned as 'NoneType' which is not iterable so one
            # can not iterate over that tuple.
            row=list(row)
            
            # Change all list entries to str (including the NoneType).
            # All NoneTypes are now 'None'-Strings
            for n in range (0, len(row)):
                line.append(str(row[n]))
                #print('{}{}'.format(n, type(line[n])))
            
            #Can stay list type.
            #line= tuple(line)
            
            print (line)
            
            # Raw content of current cell
            #cell = line[i]
            
            if 'Step' in line:
            
                for i in range(0, len(line)):
                
                    # Last cell in row
                    if i == last_column:
                        fcell = '{\\bfseries ' + line[i] + '}' + ' \\\ '
                    # All other cells
                    else:
                        fcell ='{\\bfseries ' + line[i] + '}' + ' & '
                      
                    newtablerow = newtablerow + fcell  
                
            else:
                
                for i in range (0, len(line)):
                    
                    #print('Length line:{}'.format(len(line)))
                    
                    if i == pic_column:
                    
                        snip = line[i].split(' ')
                        #print (snip)
                        # Nested loop for iterating over above defined picture
                        # in every cell.

                        for n in range(0, len(endings)):
                            for k in range(0, len(snip)):
                                # Just set a flag if 'ending' is found in cell.
                                if endings[n] in snip[k]:
                                    pic = True
                                    #print('Pic found in line {}.'.format(i))
                                    #a = GenIncludegraphics(line[i], endings)
                        
                        if pic == True:
                            #Concatenate string for all pic cell.
                            #fcell = line[i] + ' & '
                            fcell = GenIncludegraphics(snip, endings) + ' & '
                            #print(a)
                            #print('pic==True case: i={}'.format(i))
                        
                        else:
                            fcell = line[i] + ' & '
                            print('pic==False case: i={}'.format(i))
                            
                    elif i == last_column:
                        # Concatenate string for last cell in row
                        fcell = line[i] + ' \\\ '
                        print('last_column case: i={}'.format(i))
                                        
                    else:
                        #if i == 0:
                        #Concatenate string for all other cells in row
                        fcell = line[i] + ' & '
                        print('else case: i={}'.format(i))
                                 
                    newtablerow = newtablerow + fcell  
                    print(newtablerow)
                
                
            output_file.write('{}\n'.format(newtablerow))
            output_file.write('\\hline \n')
            
            # Clear variable before next loop.
            newtablerow = ''
            line = []
            pic = False
        
        print('')
        print('Done. See \'out.tex\' for output.')
                