# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 16:51:53 2021

@author: seimetz
"""

import texsheettb as ttb

file = "mrpa-assy_1111-em_log.xlsx"
endings = ['jpg', 'png']

ttb.Xlsx2Tex(file, endings)