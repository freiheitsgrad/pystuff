# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 12:50:01 2022

@author: seimetz
"""

import pandas as pd

def SubDatFrame (DFrame, MinThresh, MaxThresh):
    
    SubFrame=pd.DataFrame()
    #for i in BGSel500:
    #    print(i)
    #    indx=list(sl.loc[sl["BG"]==i].index)
    #    print (indx)
    #    sl500.append(sl.loc[indx,:])

    SubFrame = DFrame[((DFrame["BG"]>=MinThresh) & (DFrame["BG"]<=MaxThresh))]
    
    return (SubFrame)

def SubTotMargin (SubFrame):
    
    SubMargin = (SubFrame.sum(0)["GesBT_inM"] - SubFrame.sum(0)["GesBT_exM"])\
        /SubFrame.sum(0)["GesBT_exM"]
    
    return (SubMargin)

def SubSum (DFrame, ColumnStr):
    
    Sum = DFrame.sum(0)[ColumnStr]
    
    return (Sum)

def BG (DFrame):
    
    return (DFrame['BG'].drop_duplicates().tolist())
        
