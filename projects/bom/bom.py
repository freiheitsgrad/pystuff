# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 17:32:19 2023

@author: seimetz
"""

import pandas as pd
from bomtb import *

OutDict = {}
slTemp = pd.DataFrame()

# Baugruppen festlegen
BGs = [100, 400, 500, 'nan']

#sl['BG'].drop_duplicates().tolist()

xl = pd.ExcelFile("2023-07-19_ahepam_bom.xlsx")
sl = xl.parse("Stückliste")

# Sorting
sl = sl.sort_values(by="BG")
#sl['BG']=sl['BG'].astype(str)
#sl['BG']=(sl["BG"].str[:-2])

# Remove Excel's string remnants and change column to float
sl["Masse"] = (sl["Masse"].str.replace(",",".")).str[:-3]
sl["Masse"] = pd.to_numeric(sl["Masse"])

# Add calculative columns
sl["GesBT_exM"]=sl["ANZAHL"] * sl["Masse"]
sl["GesBT_inM"]=sl["GesBT_exM"] + sl["GesBT_exM"] * sl["Margin"]/100


for i in BGs:
    
    if type(i)==int:
    
        #print(i)
        slBG = SubDatFrame(sl, i, i+100)
        BGMarg = SubTotMargin(slBG)
        #SubMass_exM = SubSum(slBG, 'GesBT_exM')
        #SubMass_inM = SubSum(slBG, 'GesBT_inM')
        
        
        # print('{0:2.1f}% {1:3.3f} {2:3.3f}'.format((slMarg*100),\
        #       SubSum (slBG, 'GesBT_exM'),\
        #       SubSum (slBG, 'GesBT_inM')))
        
        # BGIdent = 'BG'+str(i)
        # MargIdent = 'Marg'+str(i)
        # SMexM_ident = 'SMexM'+str(i)
        # SMinM_ident = 'SMinM'+str(i)
        
        # OutDict['BG'+str(i)] = slBG
        # OutDict['Marg'+str(i)] = BGMarg    
        # OutDict['SMexM'+str(i)] = SubMass_exM
        # OutDict['SMinM'+str(i)] = SubMass_inM
        
        # slTemp=slBG.append({'Margin':'SubMarg: '+'{:2.1f}'.format(BGMarg*100)+'%',\
        #           'GesBT_exM':'SubMass_exM: '+'{:3.3f}'.format(SubSum (slBG, 'GesBT_exM')),\
        #           'GesBT_inM':'SubMass_inM: '+'{:3.3f}'.format(SubSum (slBG, 'GesBT_inM'))},\
        #           ignore_index=True)
    
        # OutDict['BG'+str(i)]=slTemp

        slTemp=pd.DataFrame([{'Margin':'SubMarg: '+'{:2.1f}'.format(BGMarg*100)+'%',\
                          'GesBT_exM':'SubMass_exM: '+'{:3.3f}'.format(SubSum (slBG, 'GesBT_exM')),\
                          'GesBT_inM':'SubMass_inM: '+'{:3.3f}'.format(SubSum (slBG, 'GesBT_inM'))}])    
                    
        OutDict['BG'+str(i)]=pd.concat([slBG, slTemp], ignore_index=True)

    
    else:
        pass

print('{}'.format(OutDict.keys()))
        


#bg100 = sl[sl["BG"]<200]
#other = sl[sl["BG"] == False]


#writer = pd.ExcelWriter('output.xlsx')
#df.to_excel(writer,sheet_name='Stückliste',columns=["BG"],index=False)
#sl500.to_excel(writer,sheet_name='Stückliste',index=False)
#writer.save()