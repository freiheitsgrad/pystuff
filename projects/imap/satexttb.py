# -*- coding: utf-8 -*-
"""
Created on Fri Jul 14 11:07:13 2023

@author: seimetz
"""

from math import acos, degrees, cos, radians, sqrt, tan

def AngBeta (s1, s2, s3):
    '''
    Gibt Winkel Beta (in Grad) des Kosinussatz zurück.

    Parameters
    ----------
    s1 : TYPE
        DESCRIPTION.
    s2 : TYPE
        DESCRIPTION.
    s3 : TYPE
        DESCRIPTION.

    Returns
    -------
    ang : TYPE
        DESCRIPTION.

    '''
    t = (s1**2 + s3**2 - s2**2)/ (2 * s1 * s3)
    #print(t)
    ang = degrees(acos(t))
    
    return ang

def F_S (F_G, ang):
    '''
    Gibt die Schubkraft, abhängig vom Winkel Beta zurück.

    Returns
    -------
    None.

    '''
    F = F_G * tan(radians(ang))
    
    return F

def Out (S, U):
    ''' Prints formatted value, adds given string as unit indicator'''
    print('{:1.1f} {}'.format(S, U))
    