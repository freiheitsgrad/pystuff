# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 10:20:52 2022

@author: seimetz
"""

#import numpy as np
#from datetime import datetime
#import matplotlib.pyplot as plt
#import time

def chnames_import (header_line):
    
    '''
    Extract channel names from Grant-Datalogger csv-file
    '''
    
    file= r'29140331001_semicolon.#00'
    
    i = 0
    
    with open(file, 'r') as data:
        for line in data:
                
                i+=1
                
                if i != header_line:
                    pass
                
                elif i == header_line:
                    chnames = generate_cells(line, ';')
                    
                else:
                    print('Obviously found an error case.')
    
    #print (chnames)
    return (chnames)

def generate_cells (row, delimiter):
    
    # remove newline character from imported line
    row = row.rstrip('\n')
    
    # change win decimal delimiter to '.'
    row= row.replace(',', '.')
    
    # split line into separate cells
    #  ; (semicolon) as delimiter
    cells = row.split(delimiter)
    #print(cells)
    return (cells)

def data_import ():  

    file= r'29140331001_semicolon.#00'
        
    #header_line = 26
    
    i, n = 0, 0
    
    header_line = 26
    
    chnames = chnames_import(header_line) 
    
    # Generate empty list container containing the same number of emtpy
    # lists as the number of available channels in data file.
    # Maybe not the best way, but currently I don't know better ¯\_(ツ)_/¯
    
    bins = []
    for i in range(0, len(chnames)):
        bins.append(list())
  
    i = 0
    
    with open(file, 'r') as data:
        for line in data:
                
                i+=1
                
                # Skip header
                if i <= header_line:
                    continue
                #Just use a certain number of lines (debugging)
                #elif i > 5000: break
                 
                else:
                 
                    if len(line) == 0:
                       continue
                    
                    elif 'Calibration' in line:
                        continue
                    
                    elif '#' in line:
                        continue
                    
                    # split line into separate cells
                    #  ; (semicolon) as delimiter
                    cells = generate_cells(line, ';')
                    #print(i, cells)
                    
                    for n in range(0, len(cells)):
                        
                        bins[n].append(cells[n])
                        #print('bins:{}'.format(bins[n]))
    #print(bins[0])
    return (bins, chnames)

def calc_pressure (ch1, ch2):
    
    '''
    ch1/ p1: Vordruck
    ch2/ p2: Kammerdruck
    '''
    p1, p2 = [], []
    print(p1)
    i = 0
    
    for i in range (0, len(ch1)):
        #p1: Vordruck
        p1.append(pow(10, (float(ch1[i])-6.143)/1.286))
        #p2: Kammerdruck
        p2.append(pow(10, (float(ch2[i])-7.75)/0.75))

    return (p1, p2)
                   

