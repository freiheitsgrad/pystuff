# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 10:20:52 2022

@author: seimetz
"""

from numpy import *
from datetime import datetime
import matplotlib.pyplot as plt
#import time

file= r'Z:\etph\cleanroom\tvac\testing\abpumprate.txt'
# Initialize data lists
t, p = [], []


with open(file, 'r') as data:
    for line in data:

            # remove newline character from imported line
            line = line.rstrip('\n')

            # Ignore comments (leading '#'-sign) and empty lines.
            if '#' in line:
                continue

            elif len(line) == 0:
                continue

            # split line into separate cells
            # , (colon) as delimiter
            cells = line.split(',')
            
            t.append(cells[0]); p.append(float(cells[1]))
            
    
print ('{}, {}\n{}, {}'.format(t, len(t), p, len(p)))

# make data
x = [datetime.strptime(date, "%Y-%m-%dT%H:%M") for date in t]
#x=x[:-1]
#print (x)
#x2 = [datetime.timestamp(stamp) for stamp in x]
y = p


#plt.plot(x, array(y).astype(float), '-o')
plt.plot(x, array(y), '-o')
plt.gcf().set_size_inches(10, 8)
#plt.tight_layout()
plt.xticks( rotation=25 )
plt.yscale('log')
#Set autoupdate mode
#plt.ion()
plt.show()