# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 10:20:52 2022

@author: seimetz
"""

import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import tvac_tb as tvac

P = []

# Get Data
channels, chnames = tvac.data_import()

# Convert [pressure] voltages in mbars
P= tvac.calc_pressure (channels[18], channels[19])


# Convert str to time data
#print(chnames)
x = [datetime.strptime(date, "%d.%m.%Y %H:%M:%S") for date in channels[0]]
#print(channels[0])

#x = [1, 2, 3, 4, 5, 6, 7, 9]
#y = [1, 2, 3, 4, 5, 6, 7, 9]


fig, ax = plt.subplots(figsize=(12, 8))

# 2nd y-axis
twin1 = ax.twinx()

p1, = ax.plot(x, np.array(channels[4]).astype(float), linewidth=1.0, label=chnames[4])
p2, = twin1.plot(x, np.array(P[1]).astype(float), linewidth=1.0, label='Kammerdruck als <str>!')

#plt.plot(x, np.array(channels[4]).astype(float), linewidth=1.0)

# x-axis labels
ax.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d %H:%M:%S'))
ax.xaxis.set_minor_formatter(mdates.AutoDateFormatter('%m'))
# Rotates and right-aligns the x labels so they don't crowd each other.
for label in ax.get_xticklabels(which='major'):
    label.set(rotation=30, horizontalalignment='left')

ax.set_yticks([-50, 0, 70])  # note that we don't need to specify labels
ax.yaxis.set_major_locator(ticker.MultipleLocator(10))
ax.yaxis.set_minor_locator(ticker.MultipleLocator(5))

twin1.set_yticks([1e-6, 1e-5])
twin1.yaxis.set_major_locator(ticker.MultipleLocator(5*10e-7))
#twin1.yaxis.set_minor_locator(ticker.MultipleLocator(5))


ax.set_xlabel('[Time]/ Date')  # Add an x-label to the axes.
ax.set_ylabel('[Temperature]/ $^\circ C$')  # Add a y-label to the first y-axis.
twin1.set_ylabel('[Pressure]/ mbar')  # Add a y-label to the 2nd y-axis.
ax.set_title('IuT Emtpy Bracket Test')  # Add a title to the axes.
#ax-legend()
ax.legend(handles=[p1, p2]);  # Add a legend.



plt.show()